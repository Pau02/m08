package com.example.rotations;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mediaPlayer;


    private ImageView imageView1;
    private ImageView imageView2;
    private ImageView imageView3;
    private ImageView imageView4;
    private ImageView imageView5;
    private ImageView imageView7;
    private ImageView imageView8;
    private ImageView imageView9;
    private ImageView imageView10;
    private ImageView imageView11;

    private int image1 = 0;
    private int image2 = 0;
    private int image3 = 0;
    private int image4 = 0;
    private int image5 = 0;


    ObjectAnimator objectAnimator1_1;
    ObjectAnimator objectAnimator1_2;
    ObjectAnimator objectAnimator1_3;
    ObjectAnimator objectAnimator1_4;
    ObjectAnimator objectAnimator2_1;
    ObjectAnimator objectAnimator2_2;
    ObjectAnimator objectAnimator2_3;
    ObjectAnimator objectAnimator2_4;

    ObjectAnimator objectAnimator3_1;
    ObjectAnimator objectAnimator3_2;
    ObjectAnimator objectAnimator3_3;
    ObjectAnimator objectAnimator3_4;
    ObjectAnimator objectAnimator4_1;
    ObjectAnimator objectAnimator4_2;
    ObjectAnimator objectAnimator4_3;
    ObjectAnimator objectAnimator4_4;
    ObjectAnimator objectAnimator5_1;
    ObjectAnimator objectAnimator5_2;
    ObjectAnimator objectAnimator5_3;
    ObjectAnimator objectAnimator5_4;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hook
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4 = (ImageView) findViewById(R.id.imageView4);
        imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView7 = (ImageView) findViewById(R.id.imageView7);
        imageView8 = (ImageView) findViewById(R.id.imageView8);
        imageView9 = (ImageView) findViewById(R.id.imageView9);
        imageView10 = (ImageView) findViewById(R.id.imageView10);
        imageView11 = (ImageView) findViewById(R.id.imageView11);


        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (image1 == 0 || image2 == 0 || image3 == 0 || image4 == 0 || image5 == 0) {
                    //1
                    objectAnimator1_1 = ObjectAnimator.ofFloat(imageView1, "RotationY", 0f, 180f);
                    objectAnimator1_1.setDuration(1000);
                    objectAnimator1_2 = ObjectAnimator.ofFloat(imageView1, "alpha", 1f, 0f);
                    objectAnimator1_2.setStartDelay(500);
                    //2
                    objectAnimator2_1 = ObjectAnimator.ofFloat(imageView2, "RotationY", 0f, 180f);
                    objectAnimator2_1.setDuration(1000);
                    objectAnimator2_2 = ObjectAnimator.ofFloat(imageView2, "alpha", 1f, 0f);
                    objectAnimator2_2.setStartDelay(500);
                    //3
                    objectAnimator3_1 = ObjectAnimator.ofFloat(imageView3, "RotationY", 0f, 180f);
                    objectAnimator3_1.setDuration(1000);
                    objectAnimator3_2 = ObjectAnimator.ofFloat(imageView3, "alpha", 1f, 0f);
                    objectAnimator3_2.setStartDelay(500);
                    //4
                    objectAnimator4_1 = ObjectAnimator.ofFloat(imageView4, "RotationY", 0f, 180f);
                    objectAnimator4_1.setDuration(1000);
                    objectAnimator4_2 = ObjectAnimator.ofFloat(imageView4, "alpha", 1f, 0f);
                    objectAnimator4_2.setStartDelay(500);
                    //5
                    objectAnimator5_1 = ObjectAnimator.ofFloat(imageView5, "RotationY", 0f, 180f);
                    objectAnimator5_1.setDuration(1000);
                    objectAnimator5_2 = ObjectAnimator.ofFloat(imageView5, "alpha", 1f, 0f);
                    objectAnimator5_2.setStartDelay(500);
                    //1
                    objectAnimator1_3 = ObjectAnimator.ofFloat(imageView7, "RotationY", 0f, 180f);
                    objectAnimator1_3.setDuration(1000);
                    objectAnimator1_4 = ObjectAnimator.ofFloat(imageView7, "alpha", 0f, 1f);
                    objectAnimator1_4.setStartDelay(500);
                    objectAnimator1_4.setDuration(1);
                    image1++;
                    //2
                    objectAnimator2_3 = ObjectAnimator.ofFloat(imageView8, "RotationY", 0f, 180f);
                    objectAnimator2_3.setDuration(1000);
                    objectAnimator2_4 = ObjectAnimator.ofFloat(imageView8, "alpha", 0f, 1f);
                    objectAnimator2_4.setStartDelay(500);
                    objectAnimator2_4.setDuration(1);
                    image2++;
                    //3
                    objectAnimator3_3 = ObjectAnimator.ofFloat(imageView9, "RotationY", 0f, 180f);
                    objectAnimator3_3.setDuration(1000);
                    objectAnimator3_4 = ObjectAnimator.ofFloat(imageView9, "alpha", 0f, 1f);
                    objectAnimator3_4.setStartDelay(500);
                    objectAnimator3_4.setDuration(1);
                    image3++;
                    //4
                    objectAnimator4_3 = ObjectAnimator.ofFloat(imageView10, "RotationY", 0f, 180f);
                    objectAnimator4_3.setDuration(1000);
                    objectAnimator4_4 = ObjectAnimator.ofFloat(imageView10, "alpha", 0f, 1f);
                    objectAnimator4_4.setStartDelay(500);
                    objectAnimator4_4.setDuration(1);
                    image4++;
                    //5
                    objectAnimator5_3 = ObjectAnimator.ofFloat(imageView11, "RotationY", 0f, 180f);
                    objectAnimator5_3.setDuration(1000);
                    objectAnimator5_4 = ObjectAnimator.ofFloat(imageView11, "alpha", 0f, 1f);
                    objectAnimator5_4.setStartDelay(500);
                    objectAnimator5_4.setDuration(1);
                    image5++;

            } else {
                    //1
                    objectAnimator1_1 = ObjectAnimator.ofFloat(imageView1, "RotationY", 180f, 0f);
                    objectAnimator1_1.setDuration(1000);
                    objectAnimator1_2 = ObjectAnimator.ofFloat(imageView1, "alpha", 0f, 1f);
                    objectAnimator1_2.setStartDelay(500);
                    //2
                    objectAnimator2_1 = ObjectAnimator.ofFloat(imageView2, "RotationY", 180f, 0f);
                    objectAnimator2_1.setDuration(1000);
                    objectAnimator2_2 = ObjectAnimator.ofFloat(imageView2, "alpha", 0f, 1f);
                    objectAnimator2_2.setStartDelay(500);
                    //3
                    objectAnimator3_1 = ObjectAnimator.ofFloat(imageView3, "RotationY", 180f, 0f);
                    objectAnimator3_1.setDuration(1000);
                    objectAnimator3_2 = ObjectAnimator.ofFloat(imageView3, "alpha", 0f, 1f);
                    objectAnimator3_2.setStartDelay(500);
                    //4
                    objectAnimator4_1 = ObjectAnimator.ofFloat(imageView4, "RotationY", 180f, 0f);
                    objectAnimator4_1.setDuration(1000);
                    objectAnimator4_2 = ObjectAnimator.ofFloat(imageView4, "alpha", 0f, 1f);
                    objectAnimator4_2.setStartDelay(500);
                    //5
                    objectAnimator5_1 = ObjectAnimator.ofFloat(imageView5, "RotationY", 180f, 0f);
                    objectAnimator5_1.setDuration(1000);
                    objectAnimator5_2 = ObjectAnimator.ofFloat(imageView5, "alpha", 0f, 1f);
                    objectAnimator5_2.setStartDelay(500);
                    //1
                    objectAnimator1_3 = ObjectAnimator.ofFloat(imageView7, "RotationY", 180f, 0f);
                    objectAnimator1_3.setDuration(1000);
                    objectAnimator1_4 = ObjectAnimator.ofFloat(imageView7, "alpha", 1f, 0f);
                    objectAnimator1_4.setStartDelay(500);
                    objectAnimator1_4.setDuration(1);
                    image1 = 0;
                    //2
                    objectAnimator2_3 = ObjectAnimator.ofFloat(imageView8, "RotationY", 180f, 0f);
                    objectAnimator2_3.setDuration(1000);
                    objectAnimator2_4 = ObjectAnimator.ofFloat(imageView8, "alpha", 1f, 0f);
                    objectAnimator2_4.setStartDelay(500);
                    objectAnimator2_4.setDuration(1);
                    image1 = 0;
                    //3
                    objectAnimator3_3 = ObjectAnimator.ofFloat(imageView9, "RotationY", 180f, 0f);
                    objectAnimator3_3.setDuration(1000);
                    objectAnimator3_4 = ObjectAnimator.ofFloat(imageView9, "alpha", 1f, 0f);
                    objectAnimator3_4.setStartDelay(500);
                    objectAnimator3_4.setDuration(1);
                    image3 = 0;
                    //4
                    objectAnimator4_3 = ObjectAnimator.ofFloat(imageView10, "RotationY", 180f, 0f);
                    objectAnimator4_3.setDuration(1000);
                    objectAnimator4_4 = ObjectAnimator.ofFloat(imageView10, "alpha", 1f, 0f);
                    objectAnimator4_4.setStartDelay(500);
                    objectAnimator4_4.setDuration(1);
                    image4 = 0;
                    //5
                    objectAnimator5_3 = ObjectAnimator.ofFloat(imageView11, "RotationY", 180f, 0f);
                    objectAnimator5_3.setDuration(1000);
                    objectAnimator5_4 = ObjectAnimator.ofFloat(imageView11, "alpha", 1f, 0f);
                    objectAnimator5_4.setStartDelay(500);
                    objectAnimator5_4.setDuration(1);
                    image5 = 0;
                }

                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playTogether(objectAnimator1_1, objectAnimator1_2, objectAnimator1_3, objectAnimator1_4);
                animatorSet.playTogether(objectAnimator2_1, objectAnimator2_2, objectAnimator2_3, objectAnimator2_4);
                animatorSet.playTogether(objectAnimator3_1, objectAnimator3_2, objectAnimator3_3, objectAnimator3_4);
                animatorSet.playTogether(objectAnimator4_1, objectAnimator4_2, objectAnimator4_3, objectAnimator4_4);
                animatorSet.playTogether(objectAnimator5_1, objectAnimator5_2, objectAnimator5_3, objectAnimator5_4);
                animatorSet.start();
            }

        });


    }



}