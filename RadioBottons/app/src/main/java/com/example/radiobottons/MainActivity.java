package com.example.radiobottons;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RadioGroup radioGroup = findViewById(R.id.radioGroup);
        Button option4 = findViewById(R.id.option4);
        Button option5 = findViewById(R.id.option5);
        Button option6 = findViewById(R.id.option6);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton selectedRadioButton = findViewById(checkedId);
                String text = selectedRadioButton.getText().toString();
                Toast.makeText(MainActivity.this, "Seleccionaste: " + text, Toast.LENGTH_SHORT).show();
            }
        });

        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Presionaste Option 4", Toast.LENGTH_SHORT).show();
            }
        });

        option5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Presionaste Option 5", Toast.LENGTH_SHORT).show();
            }
        });

        option6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Presionaste Option 6", Toast.LENGTH_SHORT).show();
            }
        });
    }
}